// import 'package:flutter_moi_id/moi_id.dart'; ideal way of importing
import 'package:flutter/material.dart';
import '../lib/moi_id.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'MOI ID',
        debugShowCheckedModeBanner: false,
        theme: ThemeData.dark(),
        home: SignIn(
            environment: Environment.development,
            onSuccess: (userID, inValidUser) {
              if (inValidUser) {
                // Login Successful!
                // User ID: $userID
              }
            },
            onFailure: (error) {
              // Error in authentication. Handle this
            }));
  }
}
